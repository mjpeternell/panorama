<?php
/*
 * Page Intro Content: includes WP page content.
 * Pages: Homepage,
 * 
 */
?>
<?php
if (get_field('pan_icon')) :
    $icon_bg = get_field('pan_icon');
    ?>
    <style>
        .icon-text:before {
            background:url('<?php echo $icon_bg; ?>') no-repeat 0 0;
        }
        .icon-only:before {
            background:url('<?php echo $icon_bg; ?>') no-repeat 0 0;
        }
    </style>
<?php endif; ?>
<?php
$my_id = "introSection";
?>
<section id="<?php echo $my_id; ?>" class="intro-section col-fullbleed">
    <div class="col-full">
        <div class="column-10 offset-1">
            <?php
// Start the Loop.
            while (have_posts()) : the_post();
                get_template_part('template-parts/content', 'page');
            endwhile;
            ?>
        </div>
    </div>
</section>


