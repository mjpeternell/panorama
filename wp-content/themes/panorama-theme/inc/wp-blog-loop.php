    <div class="row white">
        <div class="col-sm-7 blog-main col-md-offset-1">
            <div id="primary" class="content-area">
                <div id="content" class="site-content" role="main">

                    <?php if (have_posts()) : ?>

                        <?php rooster_park_content_nav('nav-above'); ?>

                        <?php /* Start the Loop */ ?>
                        <?php while (have_posts()) : the_post(); ?>

                            <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to overload this in a child theme then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part('content', get_post_format());
                            ?>

                        <?php endwhile; ?>

                        <?php rooster_park_content_nav('nav-below'); ?>

                    <?php else : ?>

                        <?php get_template_part('no-results', 'index'); ?>

                    <?php endif; ?>

                </div><!-- #content .site-content -->
            </div><!-- #primary .content-area -->
        </div>
        <div class="col-sm-4 blog-sidebar">
            <?php get_sidebar(); ?>
        </div>
    </div><!-- .row -->