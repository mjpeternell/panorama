<?php
/* 
 * Footer CTA: includes ACF Hero image, title and link.
 * Pages: Homepage,
 * 
 */
?>
<!-- Secondary Hero/Footer CTA -->
<div class="footer-cta">
    <?php
    $cta_image = get_field('cta_slide');

    if (!empty($cta_image)):
        $size = 'hero-cta-1400';
        $url = $cta_image['url'];
        $title = $cta_image['title'];
        $alt = $cta_image['alt'];
        $caption = $cta_image['caption'];
        $resp_size_range = "(min-width: 320px) 50vw, 100vw"; //(max-width: 90.267em) 100vw, 90.267em
        ?>
        <img srcset="<?php echo $cta_image['sizes']['hero-cta-480']; ?> 500w,<?php echo $cta_image['sizes']['hero-cta-1024']; ?> 800w, <?php echo $cta_image['sizes']['hero-cta-1024']; ?> 992w" sizes="<?php echo $resp_size_range; ?>" src="<?php echo $cta_image['sizes']['hero-cta-1400']; ?>" alt="<?php echo $alt; ?>">		
        <?php
        echo '<div class="caption">';
        if (get_field('cta_slide_title')) {
            echo '<h2><span>' . get_field('cta_slide_title') . '</span></h2>';
        }
        if (get_field('cta_slide_subtitle')) {
            echo '<h3><span>' . get_field('cta_slide_subtitle') . '</span></h3>';
        }
        if (get_field('cta_button')) {
            echo '<a id="cta_btn" href="' . get_field('cta_button') . '" class="btn btn-primary btn-lg" type="button">Let\'s Do This</a>';
        }
        echo '</div>';
        ?>
    <?php endif; ?>
</div>
