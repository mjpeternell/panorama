<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="joinTheTeam" class="col-fullbleed about-jobs">
    <div class="col-full">
        <div class="column-10 offset-1 section-title align-center">
            <h2>Join The Team</h2>
        </div>
        <?php
        $job_arg = array(
            'post_type' => 'job_postings',
            'orderby' => 'date',
            'order' => 'DESC',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );
        $wp_job_query = new WP_Query($job_arg);
        $postx_counter = -1;
        ?>
        <?php
        if (have_posts()) :
            while ($wp_job_query->have_posts()) : $wp_job_query->the_post();
                $postx_counter++;
                ?>
                <?php
                $job_description_short = get_field('job_description_short');
                ?>
                <div id="<?php echo 'job-' . $value_count; ?>" class="column-10 offset-1 value-row">
                    <div class="job-title"><?php echo the_title(); ?></div>
                    <?php if ($job_description_short): ?>
                        <div class="job-desc-short">
                            <?php echo $job_description_short; ?>
                            <div class="job-link-wrapper"><a class="job-link" href="<?php the_permalink(); ?>">Learn More</a></div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
    </div>
</section>