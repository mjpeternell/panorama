<?php
/**
 * WordPress.com-specific functions and definitions
 *
 * @package Panorama
 * @since Panorama 1.0
 */

global $themecolors;

/**
 * Set a default theme color array for WP.com.
 *
 * @global array $themecolors
 * @since Panorama 1.0
 */
$themecolors = array(
	'bg' => '',
	'border' => '',
	'text' => '',
	'link' => '',
	'url' => '',
);
