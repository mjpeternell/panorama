<?php
/*
 * Testimonial Loop: includes ACF, title, copy and link.
 * Pages: Homepage,
 */
?>
<section id="testimonials" class="col-fullbleed white text-center three-col-section">
    <div class="col-full">
        <h1 class="section-header orange">These guys think we’re pretty great.</h1>
        <div class="inner-col-full">
            <?php
            $testimonial_arg = array(
                'post_type' => 'testimonials',
                'posts_per_page' => 3,
                'orderby' => 'post_date',
                'order' => 'date',
                'post_status' => 'publish',
            );
            $wp_test_query = new WP_Query($testimonial_arg);
            $postx_counter = -1;

            while ($wp_test_query->have_posts()) : $wp_test_query->the_post();
                $postx_counter++;
                $offset = "";
                if ($postx_counter == 0) {
                    $offset = "col-lg-offset-2";
                } else {
                    $offset = "col-lg-offset-1";
                }
                ?>

                <article class="column-4 <?php //echo $offset;   ?> entry-article testimony-home" data-count="<?php echo $postx_counter; ?>">
                    <?php if (has_post_thumbnail()) { ?>
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail', array('class' => "img-circle")); ?></a>
                    <?php } else { ?>
                        <a href="<?php the_permalink(); ?>"><img src="<?php echo get_stylesheet_directory_uri() . "/assets/images/gravitar.png" ?>" class="default-thumb img-circle"/></a>
                    <?php } ?>
                    <div class="testimonial-content">
                        <?php the_content(); ?>
                    </div>
                    <?php
                    if (get_field('job_title')) {
                        $job_title = '<span>' . get_field('job_title') . '</span>';
                    }
                    if (get_field('company')) {
                        $job_company = '<span>' . get_field('company') . '</span>';
                    }
                    ?>
                    <h1 class="testimonial-title"><?php the_title('<span >', '</span>'); ?><br/><?php echo $job_title; ?>,  <?php echo $job_company; ?></h1>
                        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                </article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    </div>
</section>
