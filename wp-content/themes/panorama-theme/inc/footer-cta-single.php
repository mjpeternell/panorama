<?php
/*
 * Footer CTA: includes ACF Hero image, title and link.
 * Pages: Homepage,
 * 
 */
?>
<!-- Secondary Hero/Footer CTA -->
<?php $themeLink = get_stylesheet_directory_uri(); ?>
<div class="footer-cta bg-img">
	
<!--    <div class="caption"> </div>-->
        <div class="caption">
            <h2><span>Ready To Get Started?</span></h2>
            <a id="cta_btn" href="/" class="btn btn-primary btn-lg" type="button">Let's Do This</a>
        </div>
   
</div>
