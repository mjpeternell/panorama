<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="column-12 text-center">
                <?php if (have_posts()) : ?>
                    <header class="page-header">
                        <h1 class="page-title">The Panorama Board</h1>
                    </header><!-- .page-header -->
                <?php endif; ?>
            </div>

            <div class="column-10 offset-1 board-list">
                <?php
                $team_arg = array(
                    'post_type' => 'board_members',
                    'orderby' => 'date',
                    'order' => 'ASC',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );
                $wp_team_query = new WP_Query($team_arg);
                $postx_counter = -1;
                ?>
                <?php if (have_posts()) : ?>

                    <?php
                    while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
                        $postx_counter++;
                        ?>

                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-count="<?php echo $postx_counter; ?>">
                            <div class="arch-col-2">
                                <?php
                                if (has_post_thumbnail()) {
                                    the_post_thumbnail('thumbnail');
                                } else {
                                    echo '<img src="/wp-content/themes/panorama-theme/assets/images/gravitar.png" class="img-responsive wp-post-image"  alt="PlaceHolder Image"/>';
                                }
                                ?> 
                            </div>
                            <div class="arch-col-10">
                                <header class="entry-header">
                                    <h2 class="entry-title"><?php the_title(); ?></h2>
                                    <?php
                                    $board_member_job_title = get_field("board_member_job_title");
                                    ?>
                                    <?php if ($board_member_job_title) { ?>
                                        <div class="board-member-job-title"><?php echo $board_member_job_title; ?></div>
                                    <?php } ?>
                                </header><!-- .entry-header -->
                                <div class="entry-content">
                                    <?php //the_excerpt(); ?>
                                    <?php the_content(__('Continue reading <span class="meta-nav">&rarr;</span>', 'panorama')); ?>
                                </div><!-- .entry-content -->
                                <footer class="entry-meta">
                                    <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>', 'panorama'), '<span class="edit-link">', '</span>'); ?>
                                </footer><!-- .entry-meta -->
                            </div>
                        </article><!-- #post-<?php the_ID(); ?> -->


                    <?php endwhile; ?>

                    <div class="column-10 offset-1 align-center">
                        <a id="boardLink" href="/about-us/#board" type="button" class="back-board-link"><i class="fa fa-angle-left" aria-hidden="true"></i> Return to about page</a>
                    </div>

                <?php else : ?>



                <?php endif; ?>       
            </div>

        </div>
        <?php get_template_part('inc/panorama', 'contact-cta'); ?>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->  
<?php get_footer(); ?>