<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since Panorama 1.0
 */
?>
<?php
if (is_front_page()) {
    $icon_pg = "icon-only";
} else if ((is_page_template('page-templates/program-details.php'))) {
    $icon_pg = "icon-only";
} else {
    $icon_pg = "icon-text";
}
?>
<!-- content-page.php -->
<article id="post-<?php the_ID(); ?>" <?php post_class($icon_pg); ?>>
    <?php
    if (has_post_thumbnail()) {
        the_post_thumbnail('hero-cta-1024');
    }
    ?>
    <div class="entry-content white">
        <?php the_content(); ?>
        <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'panorama'), 'after' => '</div>')); ?>
        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
