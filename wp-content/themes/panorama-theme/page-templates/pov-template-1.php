<?php
/**
 * Template Name: POV Template - Blog 2.0
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since  Panorama 1.0
 */
get_header();
?>
<?php get_template_part('inc/hero-parallax'); ?>
<section id="PanoramaBlogFeaturedC" class="col-fullbleed panorama-blog my-search">
    <div class="col-full">
        <div class="column-10 offset-1">
            <div class="blog-column-6">
                <div class="inpage-dropdown">
                    <div class="dropdown-container">
<!--                        <p class="dropdown-description">Topics</p>-->
                        <div class="dropdown-button">Topics</div>
                        <ul class="dropdown-menu dropdown-select">
                            <?php
                            $args = array(
                                'hide_empty' => 1,
                                'number' => 10,
                                'order' => 'DESC',
                                'orderby' => 'count'
                            );

                            $terms = get_terms('pov_tags', $args);

                            if (!empty($terms) && !is_wp_error($terms)) {
                                $count = count($terms);
                                $i = 0;
                                $term_list = '';
                                foreach ($terms as $term) {
                                    $i++;
                                    $term_list .= '<li><a href="' . esc_url(get_term_link($term)) . '" alt="' . esc_attr(sprintf(__('View all post filed under %s', 'my_localization_domain'), $term->name)) . '">' . $term->name . '<span class="myCount">(' . $term->count . ')</span></a></li>';
                                }
                                echo $term_list;
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
//            echo "<pre>";
//            print_r($terms);
//            echo "</pre>";
            ?>
            <div class="blog-column-6">
                <?php get_template_part('template-parts/panorama', 'search-form'); ?>
            </div>
        </div>
    </div>
</section>
<section id="PanoramaBlogFeatured" class="col-fullbleed panorama-blog feat">
    <div class="col-full">
        <div class="column-10 offset-1">
            <?php
            $blog_arg = array(
                'post_type' => 'point_of_view',
                'orderby' => 'date',
                'order' => 'date',
                'post_status' => 'publish',
                'meta_key' => 'featured_post',
                'meta_value' => 'yes',
                'posts_per_page' => 2,
                'paged' => $paged,
            );
            $wp_blog_query = new WP_Query($blog_arg);
            $postx_counter = -1;
            if (have_posts()) :
                ?>
                <h1 class="feat-title">Featured Articles</h1>
                <?php
                while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                    $image_id = get_post_thumbnail_id();
                    $imagesize = "thumbnail";

                    $postx_counter++;
                    ?>
                    <div class="blog-column-6 blog-feature-tile" data-count="<?php echo $postx_counter; ?>">
                        <article class="tile-inner" >
                            <div class="blog-image">
                                <?php
                                if (has_post_thumbnail($wp_blog_query->ID)) {
                                    echo '<a href="' . get_permalink($wp_blog_query->ID) . '" title="' . esc_attr($wp_blog_query->post_title) . '">';
                                    echo get_the_post_thumbnail($wp_blog_query->ID, 'hero-cta-680');
                                    echo '</a>';
                                } else {
                                    echo '<a href="' . get_permalink($wp_blog_query->ID) . '" title="' . esc_attr($wp_blog_query->post_title) . '">';
                                    echo '<img src="/wp-content/themes/panorama-theme/assets/images/panorama-blog-placeholder-image.jpg" class="img-responsive"  alt="PlaceHolder Image"/>';
                                    echo '</a>';
                                }
                                ?>
                            </div>
                            <header class="entry-header">
                                <h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                                <?php
                                //$author = get_the_author();
                                //echo '<span class="author">By: ' . $author . '</span>';
                                ?> 
                                <?php
                                $post_pen_name = get_field('post_pen_name', $post->ID);
                                if ($post_pen_name) :
                                    ?>
                                    <span class="author">by <?php echo $post_pen_name; ?></span>
                                <?php else: ?>
                                    <span class="author">&nbsp;</span>
                                <?php endif; ?>
                                <?php
                                $post_source_name = get_field('post_source_name', $post->ID);
                                if ($post_source_name) :
                                    ?>
                                    <span class="author"><?php echo $post_source_name; ?></span>
                                <?php else: ?>
                                    <span class="author">&nbsp;</span>
                                <?php endif; ?>
                            </header>
                            <!--                            <div class="blog-content">
                            <?php //the_excerpt();   ?>
                                                        </div>-->

                            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<footer class="entry-meta">', '</footer>', 0, 'post-edit-link btn btn-default'); ?>

                        </article>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>

    <?php wp_reset_postdata(); ?>


</section>
<div id="PanoramaBlog" class="col-fullbleed panorama-blog">
    <div class="col-full">
        <div class="blog-column-10 offset-1">
            <?php
// for a given post type, return all
            $post_type = 'point_of_view';
            $tax = 'pov_categories';
            $tax_terms = get_terms($tax, array('orderby' => 'id', 'order' => 'ASC',));
            if ($tax_terms) {
                foreach ($tax_terms as $tax_term) {
                    $args = array(
                        'post_type' => $post_type,
                        $tax => $tax_term->slug,
                        'post_status' => 'publish',
                        'posts_per_page' => 3,
                        'orderby' => 'date',
                        'order' => 'desc',
                        'ignore_sticky_posts' => 1
                    ); // END $args
                    $my_query = null;
                    $my_query = new WP_Query($args);

                    if ($my_query->have_posts()) {
                        ?>
                        <section class="blog-column-4a blog-tile <?php echo strtolower($tax_term->name); ?>">
<!--                            <div class="cat-column-title"></div>-->
                                <div class="cat-column-title"><a href="<?php echo "/" . $tax_term->taxonomy . "/" . $tax_term->slug; ?>"><?php echo $tax_term->name; ?></a></div>
                            
                            <?php
                            while ($my_query->have_posts()) : $my_query->the_post();
                                ?>
                                <article class="tile-inner">

                                    <?php //the_post_thumbnail();    ?>
                                    <header class="entry-header">
                                        <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                    </header>
                                                                        <div class="blog-content">
                                    <?php the_excerpt();    ?>
                                                                        </div>
                             
                                    <footer>
                                        <?php
                                        //$author = get_the_author();
                                        //echo '<span class="author">By: ' . $author . '</span>';
                                        ?> 
                                        <?php
                                        $post_pen_name = get_field('post_pen_name', $post->ID);
                                        if ($post_pen_name) :
                                            ?>
                                            <span class="author">by <?php echo $post_pen_name; ?></span>
                                        <?php else: ?>
                                            <span class="author">&nbsp;</span>
                                        <?php endif; ?>
                                        <?php
                                        $post_source_name = get_field('post_source_name', $post->ID);
                                        if ($post_source_name) :
                                            ?>
                                            <span class="author"><?php echo $post_source_name; ?></span>
                                        <?php else: ?>
                                            <span class="author">&nbsp;</span>
                                        <?php endif; ?>
                                        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '', '', 0, 'post-edit-link btn btn-default'); ?>

                                    </footer>
                                </article>
                            <?php endwhile; ?>
                            <div class="cat-column-footer">
                                <a href="<?php echo "/" . $tax_term->taxonomy . "/" . $tax_term->slug; ?>">view all <?php echo $tax_term->name; ?></a>
                            </div>
                        </section>
                        <?php
                    } // END if have_posts loop
                    wp_reset_query();
                } // END foreach $tax_terms
            } // END if $tax_terms
            ?>
        </div>
    </div>
</div>

<?php get_template_part('inc/panorama', 'contact-cta'); ?>
<?php
get_footer();
