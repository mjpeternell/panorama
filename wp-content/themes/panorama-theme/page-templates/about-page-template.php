<?php
/**
 * Template Name: About Page Template
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax'); 
?>
    <?php get_template_part('inc/page-template-intro-content'); ?>
    <?php get_template_part('inc/about', 'our-values'); ?>
    <?php //get_template_part('inc/about', 'founder'); ?>
    <?php //get_template_part('inc/about', 'quote'); ?> 
    <?php get_template_part('inc/about', 'team-members'); ?>
    <?php //get_template_part('inc/about', 'jobs'); ?>
    <?php get_template_part('inc/about', 'advisors'); ?>
    <?php get_template_part('inc/about', 'board-members'); ?>
    <?php get_template_part('inc/panorama', 'contact-cta'); ?>
<?php
get_footer();


