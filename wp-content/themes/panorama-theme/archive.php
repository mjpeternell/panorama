<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();

$page_color = "";
if (is_tax('pov_categories', 'productivity')) {
    $page_color = "honey";
} elseif (is_tax('pov_categories', 'people')) {
    $page_color = "plum";
}
if (is_tax('pov_categories', 'planet')) {
    $page_color = "forest";
}
if (is_tax('pov_tags')) {
    $page_color = "ocean";
}
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="col-full">
                <div class="column-12 text-center">
                    <?php if (have_posts()) : ?>
                        <header class="page-header <?php echo $page_color; ?>">
                            <h1 class="page-title trunc-125">
                                <?php
                                if (is_category()) {
                                    printf(__('Category Archives: %s', 'panorama'), '<span>' . single_cat_title('', false) . '</span>');
                                } elseif (is_tag()) {
                                    printf(__('Tag Archives: %s', 'panorama'), '<span>' . single_tag_title('', false) . '</span>');
                                } elseif (is_author()) {
                                    /* Queue the first post, that way we know
                                     * what author we're dealing with (if that is the case).
                                     */
                                    the_post();
                                    printf(__('Author Archives: %s', 'panorama'), '<span class="vcard"><a class="url fn n" href="' . get_author_posts_url(get_the_author_meta("ID")) . '" title="' . esc_attr(get_the_author()) . '" rel="me">' . get_the_author() . '</a></span>');
                                    /* Since we called the_post() above, we need to
                                     * rewind the loop back to the beginning that way
                                     * we can run the loop properly, in full.
                                     */
                                    rewind_posts();
                                } elseif (is_day()) {
                                    printf(__('Daily Archives: %s', 'panorama'), '<span>' . get_the_date() . '</span>');
                                } elseif (is_month()) {
                                    printf(__('Monthly Archives: %s', 'panorama'), '<span>' . get_the_date('F Y') . '</span>');
                                } elseif (is_year()) {
                                    printf(__('Yearly Archives: %s', 'panorama'), '<span>' . get_the_date('Y') . '</span>');
                                } elseif (is_tax('pov_categories')) {
                                    printf(__('%s', 'panorama'), '<span>' . single_term_title() . '</span>');
                                } elseif (is_tax('pov_tags')) {
                                    printf(__('%s', 'panorama'), '<span>' . single_term_title() . '</span>');
                                } else {
                                    _e('Archives', 'panorama');
                                }
                                ?>
                            </h1>
                            <?php
                            if (is_category()) {
                                // show an optional category description
                                $category_description = category_description();
                                if (!empty($category_description))
                                    echo apply_filters('category_archive_meta', '<div class="taxonomy-description">' . $category_description . '</div>');
                            } elseif (is_tag()) {
                                // show an optional tag description
                                $tag_description = tag_description();
                                if (!empty($tag_description))
                                    echo apply_filters('tag_archive_meta', '<div class="taxonomy-description">' . $tag_description . '</div>');
                            }
                            ?>
                        </header><!-- .page-header -->
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-fullbleed">
                <div class="col-full">
                    <div class="column-10 offset-1">
                        <ul class="pov-cat-nav">
                            <li class="goTo"><span>Go to:</span></li>
                            <li><a href="/point-of-view/">Point of View</a></li>
                            <li><a href="/pov_categories/planet/">Planet</a></li>
                            <li><a href="/pov_categories/people/">People</a></li>
                            <li><a href="/pov_categories/productivity/">Productivity</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-fullbleed panorama-blog">
                <div class="col-full">
                    <div class="column-10 offset-1">
                        <?php if (have_posts()) : ?>


                            <?php //rooster_park_content_nav('nav-above'); ?>

                            <?php /* Start the Loop */ ?>
                            <?php while (have_posts()) : the_post(); ?>

                                <?php
                                /* Include the Post-Format-specific template for the content.
                                 * If you want to overload this in a child theme then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part('content', get_post_format());
                                ?>

                            <?php endwhile; ?>

                            <?php panorama_content_nav('nav-below'); ?>

                        <?php else : ?>

                            <?php get_template_part('template-parts/no-results', 'archive'); ?>

                        <?php endif; ?>       
                    </div>
                </div>
            </div>
        </div>
        <?php get_template_part('inc/panorama', 'contact-cta'); ?>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->  
<?php get_footer(); ?>