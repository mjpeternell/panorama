/*! Panorama - panorama.js - v0.1.0 - 2016-04-01
 * http://panoramaDotcom
 * Copyrigh0;
 jQuery(document).ready(function () {t (c) 2016; * Licensed GPLv2+ */
jQuery(window).load(function () {

    //dropdown POV overview page
    jQuery(".dropdown-button").click(function () {
        var $button, $menu;
        $button = $(this);
        $menu = $button.siblings(".dropdown-menu");
        $menu.toggleClass("show-menu");
        $menu.children("li").click(function () {
            $menu.removeClass("show-menu");
            $button.html($(this).html());

        });
        if ($menu.hasClass("show-menu")) {
            setTimeout(function () {
                $menu.removeClass("show-menu");
            }, 9500);
        }
    });

    var val_speed = 250;
    var id = "";
    var id_text = "";
    jQuery(".value-block").hover(function () {
        id = jQuery(this).find('.val-img');
        id_text = jQuery(this).find('.value-tagline');
        jQuery(this).toggleClass('alt-color');
        //jQuery(id).toggle(val_speed);
        jQuery(id).toggleClass('hide-icon');
        jQuery(id_text).toggleClass('show-me');
    });

    jQuery("#Header").headroom({
        "offset": 150,
//        tolerance: {
//            up: 0,
//            down: 100
//        },
        "classes": {
            // when element is initialised
            "initial": "headroom",
            // when scrolling up
            "pinned": "headroom--pinned",
            // when scrolling down
            "unpinned": "headroom--unpinned",
            // when above offset
            "top": "headroom--top",
            // when below offset
            "notTop": "headroom--not-top",
            // when at bottom of scoll area
            "bottom": "headroom--bottom",
            // when not at bottom of scroll area
            "notBottom": "headroom--not-bottom"
        }
    });
});


jQuery(document).ready(function () {

    jQuery('#pageAuxNav ul').each(function () {
        // Will replace "<ul></ul>" by "<p>Sorry, this is empty</p>"
        if (!jQuery(this).has('li').length) {
            jQuery('#pageAuxNav').hide();
        }
    });

    //Check current widow size for show/hide cloned nav menu.
    var scroll_window_w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;



    /** In View https://github.com/protonet/jquery.inview **/
    jQuery('#myCounter').on('inview', function (event, isInView) {
        if (isInView) {
            //console.log("Counter in view");
            /* === COUNT FACTORS - https://github.com/mhuggins/jquery-countTo === */
            var dataperc;
            jQuery('.fact').each(function () {
                dataperc = jQuery(this).attr('data-perc'),
                        jQuery(this).find('.factor').delay(10000).countTo({
                    from: 0,
                    to: dataperc,
                    speed: 3000,
                    refreshInterval: 50
                });
            });
        } else {
            //console.log("Counter Not In View");
        }
    });

    //truncate stings    
    jQuery('.blog-content p').succinct({
        size: 90
    });
    jQuery('#PanoramaBlog .title a').succinct({
        size: 125
    });
    jQuery('#PanoramaBlogFeatured .title a').succinct({
        size: 125
    });
    jQuery('.entry-header .entry-title a').succinct({
        size: 90
    });
    jQuery('.trunc-500').succinct({
        size: 500
    });
    jQuery('.trunc-275').succinct({
        size: 275
    });
    jQuery('.trunc-250').succinct({
        size: 250
    });
    jQuery('.trunc-225').succinct({
        size: 225
    });
    jQuery('.trunc-200').succinct({
        size: 200
    });
    jQuery('.trunc-175').succinct({
        size: 175
    });
    jQuery('.trunc-150').succinct({
        size: 150
    });
    jQuery('.trunc-125').succinct({
        size: 125
    });
    jQuery('.trunc-120').succinct({
        size: 120
    });
    jQuery('.trunc-115').succinct({
        size: 115
    });
    jQuery('.trunc-100').succinct({
        size: 100
    });

    jQuery('#PanoramaBlog .excerpt').succinct({
        size: 115
    });



    jQuery('#CaseStudiesHome .content-container p').succinct({
        size: 80
    });

    //scroll to top functionality
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-to-top').fadeIn();
        } else {
            jQuery('.scroll-to-top').fadeOut();
        }
    });


    //scroll to top functionality Scroll to top of page
    jQuery(window).scroll(function () {
        var my_docHeight = jQuery(document).height();
        var my_winHeight = jQuery(window).height();
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-to-top').fadeIn();
        } else {
            jQuery('.scroll-to-top').fadeOut();
        }
        if (jQuery(this).scrollTop() + my_winHeight === my_docHeight) {
            jQuery('.scroll-to-top').fadeOut();
        }
    });

    //Click event to scroll to top
    jQuery('.scroll-to-top').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    window.onorientationchange = function () {
        window.location.reload();
    };

//triggered when modal is about to be shown
    jQuery('.mod-pop button').on('click', function (e) {
        //get data-id attribute of the clicked element

        //populate the textbox

    });

    jQuery('a[name=\'modal\']').click(function (e) {

        //Cancel the link behavior
        e.preventDefault();
        //Get the A tag
        var id = jQuery(this).attr('href');

        //Get the window height and width
        var winH = jQuery(window).height();
        var winW = jQuery(window).width();
        //console.log(winH + ' - ' + winW);
        //Set the popup window to center
        if (winW <= 680) {
            jQuery(id).css('top', 40);
        } else {
            jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
        }
        //jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
        jQuery(id).css('left', winW / 2 - jQuery(id).width() / 2);

        //var theTitle = jQuery(this).siblings(".entry-header").children('h1').find('a').text();
        var theTitle = jQuery(this).siblings('.popup-title').html();
        var theContent = jQuery(this).siblings(".popup-content").html();
        console.log(theTitle);
        console.log(theContent);
        jQuery(id).find('header h2').append(theTitle);
        jQuery(id).find('.content').append(theContent);

        //transition effect
        jQuery('#modal_screen').fadeIn(500);
        jQuery(id).fadeIn(500);

    });

//if close button is clicked
    jQuery('.modalwindow .close-btn').click(function (e) {
        console.log(e);
        var mod_id = jQuery('.modalwindow');
        jQuery('.modalwindow').find('header h2').empty();
        jQuery('.modalwindow').find('.content p').remove();
        //Cancel the link behavior
        e.preventDefault();

        jQuery('.modalwindow').fadeOut(500);
        jQuery('#modal_screen').fadeOut(500);
    });

//    jQuery(window).resize(function () {
//        var more = document.getElementById("js-navigation-more");
//        if (jQuery(more).length > 0) {
//            var windowWidth = $(window).width();
//            var moreLeftSideToPageLeftSide = jQuery(more).offset().left;
//            var moreLeftSideToPageRightSide = windowWidth - moreLeftSideToPageLeftSide;
//
//            if (moreLeftSideToPageRightSide < 330) {
//                jQuery("#js-navigation-more .submenu .submenu").removeClass("fly-out-right");
//                jQuery("#js-navigation-more .submenu .submenu").addClass("fly-out-left");
//            }
//
//            if (moreLeftSideToPageRightSide > 330) {
//                jQuery("#js-navigation-more .submenu .submenu").removeClass("fly-out-left");
//                jQuery("#js-navigation-more .submenu .submenu").addClass("fly-out-right");
//            }
//        }
//    });


    var menuToggle = jQuery("#js-mobile-menu").unbind();

    menuToggle.on("click", function (e) {
        e.preventDefault();
        jQuery("body").toggleClass("nav-open");
        jQuery(".navigation-wrapper").toggleClass("nav-expand");

        jQuery("#js-navigation-menu").slideToggle(function () {
            jQuery("#navbar").toggleClass("scroll-y");
            //jQuery("#js-navigation-menu").toggleClass("show");
        });
    });


    //Smooth Scorll Functionality
    // Scroll To Functionality
    jQuery(function () {
        // your current click function
        jQuery('#hero-btn, .scroll').on('click', function (e) {
            e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: jQuery(jQuery(this).attr('href')).offset().top + 'px'
            }, 1000, 'swing');
        });


        jQuery('#pageAuxNav a[href*="#"]:not([href="#"]), #teamLink[href*="#"]:not([href="#"]), #js-navigation-menu .dropdown-menu li a[href*="#"]:not([href="#"])').click(function () {
            if (window.location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && window.location.hostname === this.hostname) {
                var target = jQuery(this.hash);

                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    jQuery('html,body').animate({
                        scrollTop: (target.offset().top)
                    }, 1000);
                    return false;
                }
                console.log(target);
            }
        });

        // *only* if we have anchor on the url
        if (window.location.hash) {
            // smooth scroll to the anchor id
            jQuery('html, body').animate({
                scrollTop: jQuery(window.location.hash).offset().top + 'px'
            }, 1000, 'swing');
        }

    });

});