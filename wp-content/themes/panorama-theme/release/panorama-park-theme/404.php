<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
$page_color = "";
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="col-full">
                <div class="column-12 text-center">
                    <header class="page-header <?php echo $page_color; ?>">
                        <h1 class="page-title"><?php _e('Oops! That page can&rsquo;t be found.', 'panorama'); ?></h1>
                    </header>
                </div>
            </div>
        </div>
        <div class="col-fullbleed  white">
            <div class="col-full">
                <div class="column-10 offset-1 error-section">
                    <p><?php _e('It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'panorama'); ?></p>
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>

    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_footer(); ?>
