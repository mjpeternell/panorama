<?php
/**
 * @package Panorama
 * @since Panorama 1.0
 */
?>
<?php
$my_arch_cat = strtolower(single_term_title("", false));
$my_post_class = "";
$my_tax = "";
if (is_tax('pov_tags')) {
    $my_tax = "pov-tags";
}

$my_post_class = array('blog-column-4aa', 'blog-tile', $my_arch_cat, $my_tax);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($my_post_class); ?>>
    <div class="tile-inner">
        <header class="entry-header">
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" class="truncate-120" title="<?php echo esc_attr(sprintf(__('Permalink to %s', 'panorama'), the_title_attribute('echo=0'))); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
        </header><!-- .entry-header -->
        <?php if (is_search()) : // Only display Excerpts for Search    ?>
            <!-- <div class="entry-summary">
            <?php //the_excerpt();  ?>
             </div>-->
        <?php else : ?>
            <div class="blog-content">
                <?php the_excerpt(); ?>
            </div><!-- .entry-content -->
            <footer>
                <?php //panorama_posted_on();  ?>
                <?php
                $post_pen_name = get_field('post_pen_name', $post->ID);
                if ($post_pen_name) :
                    ?>
                    <span class="author">by <?php echo $post_pen_name; ?></span>
                <?php else: ?>
                    <span class="author">&nbsp;</span>
                <?php endif; ?>
                <?php
                $post_source_name = get_field('post_source_name', $post->ID);
                if ($post_source_name) :
                    ?>
                    <span class="author"><?php echo $post_source_name; ?></span>
                <?php else: ?>
                    <span class="author">&nbsp;</span>
                <?php endif; ?>
            </footer>
        <?php endif; ?>

        <footer class="entry-meta">
            <?php if (in_array(get_post_type(), array('post', 'team_members', 'board_members', 'point_of_view'))) : // Hide category and tag text for pages on Search  ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list(__(', ', 'panorama'));
                if ($categories_list && panorama_categorized_blog()) :
                    ?>
                    <span class="cat-links">
                        <?php printf(__('Posted in %1$s', 'panorama'), $categories_list, '<span class="sep"> | </span>'); ?>
                    </span>
                <?php endif; // End if categories    ?>

                <?php
                //                   echo '<span class="tags-links">';
//                if (is_tax('pov_tags') || is_tax('pov_categories')) {
//                    //echo get_the_term_list($post->ID, 'pov_tags', 'Topics: ', ', ');
//                }
//                    echo '</span>'
                ?>
                <?php
                /* translators: used between list items, there is a space after the comma */
//                if (!is_tax()) :
//                    $tags_list = get_the_tag_list('', __(', ', 'panorama'));
//                    if ($tags_list) :
//                        
                ?>
            <!--<span class="tags-links">
                <?php
//                            printf(__('Tagged: %1$s', 'panorama'), $tags_list);
//                            
                ?>
            </span>-->
                <?php
//                    endif;
//                endif;
                ?>
            <?php endif; // End if 'post' == get_post_type()    ?>

            <?php if (!is_tax()) : ?>
    <?php if (!post_password_required() && ( comments_open() || '0' != get_comments_number() )) : ?>
                    <span class="sep"> | </span>
                    <span class="comments-link"><?php comments_popup_link(__('Leave a comment', 'panorama'), __('1 Comment', 'panorama'), __('% Comments', 'panorama')); ?></span>
                <?php endif; ?>
            <?php endif; ?>
<?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>', 'panorama'), '<span class="edit-link">', '</span>'); ?>
        </footer><!-- .entry-meta -->
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
