<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="single-hero">
                        <h1 class="single-title"><?php the_title(); ?></h1>
                        <div class="single-subtitle"><?php the_title(); ?></div>
                        
                    </header><!-- .entry-header -->
                    <div class="column-8 offset-2 single-job-post">

                        <div class="entry-content">
                            
                            <?php the_content(); ?>
                            <?php
                            $job_description_long = get_field("job_description_long");
                            ?>
                            <?php if ($job_description_long) { ?>
                                <?php echo $job_description_long; ?>
                            <?php } ?>
                            <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'panorama'), 'after' => '</div>')); ?>
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
                            <?php edit_post_link(__('Edit', 'panorama'), '<span class="edit-link">', '</span>'); ?>
                        </footer><!-- .entry-meta -->
                    </div>
                    <div class="column-8 offset-2 align-center">
                        <div class="single-team-nav bottom">
                            <a id="teamLink" href="/about/#joinTheTeam" type="button" class=""><i class="fa fa-angle-left" aria-hidden="true"></i> Back To Jobs</a>
                        </div>
                    </div>
                </article><!-- #post-<?php the_ID(); ?> -->
            <?php endwhile; // end of the loop.  ?>
        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php
//get_template_part('inc/footer-cta-single');
get_footer();
?>