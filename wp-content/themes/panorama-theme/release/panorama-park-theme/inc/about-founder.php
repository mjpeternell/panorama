<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="col-fullbleed about-founder">
    <div class="col-full">
        <div class="column-10 offset-1">
            <?php
            $founder_photo = get_field("founder_photo");
            $founder_name = get_field("founder_name");
            $founder_title = get_field("founder_title");
            $founder_bio = get_field("founder_bio");
            ?>
            <?php
            if ($founder_photo) { ?>
            <div class="founder-photo"><img src="<?php echo $founder_photo['url']; ?>" alt="<?php echo $founder_photo['alt']; ?>"/></div>
            <?php } ?>
            <?php
            if ($founder_name) { ?>
            <div class="founder-name"><?php echo $founder_name; ?></div>
            <?php } ?>
             <?php
            if ($founder_title) { ?>
            <div class="founder-title"><?php echo $founder_title; ?></div>
            <?php } ?>
            <?php
            if ($founder_bio) { ?>
            <div class="founder-bio entry-content"><?php echo $founder_bio; ?></div>
            <?php } ?>
        </div>
    </div>
</section>