<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$team_member_title = "";
?>
<section id="teamAdvisors" class="col-fullbleed about-team-members advis">
    <div class="col-full">
        <div class="column-10 offset-1 section-title align-center">
            <h2>Our Advisors</h2>
        </div>
        <div class="column-10 offset-1">
            <div class="team-row">
                <?php
                $team_arg = array(
                    'post_type' => 'team_members',
                    'team_category' => 'advisor',
                    'orderby' => 'title',
                    'order' => 'DESC',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );
                $wp_team_query = new WP_Query($team_arg);
                $postx_counter = -1;
                if (have_posts()) :
                    while ($wp_team_query->have_posts()) : $wp_team_query->the_post();
                        $image_id = get_post_thumbnail_id();
                        $imagesize = "thumbnail";

                        $postx_counter++;
                        $offset = "";
                        if ($postx_counter == 0) {
                            $offset = "col-lg-offset-2";
                        } else {
                            $offset = "col-lg-offset-1";
                        }
                        ?>
                        <div class="team-tile" data-count="<?php echo $postx_counter; ?>">
                            <article class="tile-inner" >
                                <div class="team-image">
                                    <?php
                                    if (has_post_thumbnail($wp_team_query->ID)) {
                                        echo '<a href="' . get_permalink($wp_team_query->ID) . '" title="' . esc_attr($wp_team_query->post_title) . '">';
                                        echo get_the_post_thumbnail($wp_team_query->ID, 'hero-cta-680');
                                        echo '</a>';
                                    } else {
                                        echo '<a href="' . get_permalink($wp_team_query->ID) . '" title="' . esc_attr($wp_team_query->post_title) . '">';
                                        echo '<img src="/wp-content/themes/panorama-theme/assets/images/gravitar.png" class="img-responsive"  alt="PlaceHolder Image"/>';
                                        echo '</a>';
                                    }
                                    ?>
                                </div>
                                <div class="team-content">
                                    <div class="member-name">
                                        <?php the_title(); ?>
                                    </div>
                                    <?php
                                    $team_member_short_bio = get_field("team_member_short_bio");
                                    ?>
                                    <?php if ($team_member_short_bio) { ?>
                                        <div class="member-short-bio trunc-100"><?php echo $team_member_short_bio; ?></div>
                                    <?php } ?>
                                    <div class="link-wrapper">
                                        <a class="member-link" href="<?php the_permalink(); ?>">Read More</a>
                                        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '', '', 0, 'post-edit-link'); ?>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
</section>