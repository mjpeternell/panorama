<?php

/**
 * RoosterPark Social Sharing Buttons
 *
 * @package Panorama
 * @since RoosterPark 1.2
 */
//function social_sharing_buttons($content) {
//    global $post;
//    if (is_singular($post_types = '') || is_home()) {
//
//        // Get current page URL 
//        $socialURL = urlencode(get_permalink());
//
//        // Get current page title
//        $crunchifyTitle = str_replace(' ', '%20', get_the_title());
//
//        // Get Post Thumbnail for pinterest
//        $crunchifyThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
//
//        // Construct sharing URL without using any script
//        $twitterURL = 'https://twitter.com/intent/tweet?text=' . $crunchifyTitle . '&amp;url=' . $socialURL . '&amp;via=Crunchify';
//        //$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$socialURL;
//        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $socialURL . '&amp;title=' . $crunchifyTitle;
////		$googleURL = 'https://plus.google.com/share?url='.$socialURL;
////		$bufferURL = 'https://bufferapp.com/add?url='.$socialURL.'&amp;text='.$crunchifyTitle;
////		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$socialURL.'&amp;title='.$crunchifyTitle;
////		$whatsappURL = 'whatsapp://send?text='.$crunchifyTitle . ' ' . $socialURL;
//        // Based on popular demand added Pinterest too
//        $pinterestURL = 'https://pinterest.com/pin/create/button/?url=' . $socialURL . '&amp;media=' . $crunchifyThumbnail[0] . '&amp;description=' . $crunchifyTitle;
//        $variable = '';
//        // Add sharing button at the end of page/page content
//        $variable .= '<div class="share-social"><ul>';
//        $variable .= '<li><span class="share-text">Share</span></li>';
//        $variable .= '<li><a class="share-link share-facebook" href="' . $facebookURL . '" target="_blank" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>';
//        $variable .= '<li><a class="share-link share-twitter" href="' . $twitterURL . '" target="_blank" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>';
////		$variable .= '<a class="share-link share-whatsapp" href="'.$whatsappURL.'" target="_blank">WhatsApp</a>';
////		$variable .= '<a class="share-link share-googleplus" href="'.$googleURL.'" target="_blank">Google+</a>';
////		$variable .= '<a class="share-link share-buffer" href="'.$bufferURL.'" target="_blank">Buffer</a>';
////		$variable .= '<a class="share-link share-linkedin" href="'.$linkedInURL.'" target="_blank">LinkedIn</a>';
////		$variable .= '<a class="share-link crunchify-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank">Pin It</a>';
//        $variable .= '</ul></div>';
//
//        return $variable . $content;
//    } else {
//        // if not a post/page then don't include sharing button
//        //return $variable.$content;
//    }
//}
//
//add_filter('the_content', 'social_sharing_buttons');

//[foobar]
function foobar_func($content) {
    global $post;
    if (is_singular($post_types = '') || is_home()) {

        // Get current page URL 
        $socialURL = urlencode(get_permalink());

        // Get current page title
        $crunchifyTitle = str_replace(' ', '%20', get_the_title());

        // Get Post Thumbnail for pinterest
        $crunchifyThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

        // Construct sharing URL without using any script
        $twitterURL = 'https://twitter.com/intent/tweet?text=' . $crunchifyTitle . '&amp;url=' . $socialURL . '&amp;via=Crunchify';
        //$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$socialURL;
        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $socialURL . '&amp;title=' . $crunchifyTitle;
//		$googleURL = 'https://plus.google.com/share?url='.$socialURL;
//		$bufferURL = 'https://bufferapp.com/add?url='.$socialURL.'&amp;text='.$crunchifyTitle;
//		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$socialURL.'&amp;title='.$crunchifyTitle;
//		$whatsappURL = 'whatsapp://send?text='.$crunchifyTitle . ' ' . $socialURL;
        // Based on popular demand added Pinterest too
        $pinterestURL = 'https://pinterest.com/pin/create/button/?url=' . $socialURL . '&amp;media=' . $crunchifyThumbnail[0] . '&amp;description=' . $crunchifyTitle;
        $variable = '';
        // Add sharing button at the end of page/page content
        $variable .= '<div class="share-social"><ul>';
        $variable .= '<li><span class="share-text">Share</span></li>';
        $variable .= '<li><a class="share-link share-facebook" href="' . $facebookURL . '" target="_blank" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>';
        $variable .= '<li><a class="share-link share-twitter" href="' . $twitterURL . '" target="_blank" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>';
//		$variable .= '<a class="share-link share-whatsapp" href="'.$whatsappURL.'" target="_blank">WhatsApp</a>';
//		$variable .= '<a class="share-link share-googleplus" href="'.$googleURL.'" target="_blank">Google+</a>';
//		$variable .= '<a class="share-link share-buffer" href="'.$bufferURL.'" target="_blank">Buffer</a>';
//		$variable .= '<a class="share-link share-linkedin" href="'.$linkedInURL.'" target="_blank">LinkedIn</a>';
//		$variable .= '<a class="share-link crunchify-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank">Pin It</a>';
        $variable .= '</ul></div>';

        return $variable . $content;
    }
}

add_shortcode('foobar', 'foobar_func');
