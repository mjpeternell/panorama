<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section class="col-fullbleed about-quote">
    <div class="col-full">
        <div class="column-10 offset-1">
            <?php
            $about_quote = get_field("about_quote");
            ?>
            <?php
            if ($about_quote) { ?>
            <div class="entry-content"><?php echo $about_quote; ?></div>
            <?php } ?>
        </div>
    </div>
</section>