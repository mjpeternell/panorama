<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="values" class="col-fullbleed about-values">
    <div class="col-full">
        <div class="column-10 offset-1 section-title align-center">
            <?php
            $about_logo = get_field('about_logo');
            if ($about_logo):
                ?>
                <img class="value-logo" src="<?php echo $about_logo['url']; ?>" alt="<?php echo $about_logo['alt'] ?>" />
            <?php endif; ?>
        </div>
        <?php
        $about_values_intro = get_field("about_values_intro");
        ?>
        <?php if ($about_values_intro) { ?>
            <div class="column-8 offset-2 about-intro">
                <?php echo $about_values_intro; ?>
            </div>
        <?php } ?>
        <div class="column-12 value-row">
            <?php if (have_rows('about_values_repeater')): ?>
                <?php
                $value_count = 0;
                while (have_rows('about_values_repeater')): the_row();
                    // vars
                    $value_icon = get_sub_field('value_icon');
                    $value_title = get_sub_field('value_title');
                    $value_tagline = get_sub_field('value_tagline');
                    $value_count++;
                    ?>
                    <div id="<?php echo 'block' . $value_count; ?>" class="value-block align-center">
                        <?php if ($value_title): ?>
                            <div class="value-title "><?php echo $value_title; ?></div>
                        <?php endif; ?>
                        <div id="<?php echo 'subBlock' . $value_count; ?>" class="featured-icon">
                            <?php if ($value_icon): ?>
                                <img class="value-animate val-img" src="<?php echo $value_icon['url']; ?>" alt="<?php echo $value_icon['alt'] ?>" />
                                <div class="value-animate value-tagline"><?php echo $value_tagline; ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>