<section id="board" class="col-fullbleed about-team-members boardmem">
    <div class="col-full">
        <div class="column-10 offset-1 section-title align-center">
            <h2>Our Board</h2>
        </div>

        <div class="column-10 offset-1">
            <div class="team-row">
                <?php
                $team_ceo_arg = array(
                    'post_type' => 'board_members',
                    'orderby' => 'date',
                    'order' => 'ASC',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                );
                $wp_team_ceo_query = new WP_Query($team_ceo_arg);
                $postx_counter = -1;
                if (have_posts()) :
                    while ($wp_team_ceo_query->have_posts()) : $wp_team_ceo_query->the_post();
                        $image_id = get_post_thumbnail_id();
                        $imagesize = "thumbnail";

                        $postx_counter++;
                        $offset = "";
                        if ($postx_counter == 0) {
                            $offset = "col-lg-offset-2";
                        } else {
                            $offset = "col-lg-offset-1";
                        }
                        ?>
                        <div class="team-tile-brd" data-count="<?php echo $postx_counter; ?>">
                            <article class="tile-inner" >
                                <div class="team-image">
                                    <?php   
                                    if (has_post_thumbnail($wp_team_ceo_query->ID)) {
                                        echo '<a href="' . get_permalink($wp_team_ceo_query->ID) . '" title="' . esc_attr($wp_team_ceo_query->post_title) . '">';
                                        echo get_the_post_thumbnail($wp_team_ceo_query->ID, 'hero-cta-680');
                                        echo '</a>';
                                    } else {
                                        echo '<a href="' . get_permalink($wp_team_ceo_query->ID) . '" title="' . esc_attr($wp_team_ceo_query->post_title) . '">';
                                        echo '<img src="/wp-content/themes/panorama-theme/assets/images/gravitar.png" class="img-responsive"  alt="PlaceHolder Image"/>';
                                        echo '</a>';
                                    }
                                    ?>
                                </div>
                                <div class="team-content">
                                    <div class="member-name">
                                        <?php the_title(); ?>
                                    </div>

                                    <?php
                                    $board_member_short_bio = get_field("board_member_short_bio");
                                    ?>
                                    <?php if ($board_member_short_bio) { ?>
                                        <div class="member-short-bio trunc-100"><?php echo $board_member_short_bio; ?></div>
                                    <?php } ?>
                                    <div class="link-wrapper">
                                        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '', '', 0, 'post-edit-link'); ?>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>
        <div class="column-10 offset-1 align-center">
            <a id="boardLink" href="/board-members/" type="button" class="board-link"><i class="fa fa-angle-right" aria-hidden="true"></i> Learn more about our board</a>
        </div>
    </div>
</section>