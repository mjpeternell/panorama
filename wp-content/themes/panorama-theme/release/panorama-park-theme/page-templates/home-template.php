<?php
/**
 * Template Name: Home Page
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since  Panorama 1.0
 */
get_header();
get_template_part('inc/hero-parallax'); 
?>
    <?php get_template_part('inc/page-template-intro-content'); ?>
    <?php get_template_part('inc/panorama', 'case-studies'); ?>
    <?php get_template_part('inc/panorama', 'partners'); ?>
    <?php get_template_part('inc/panorama', 'contact-cta'); ?>
<?php
get_footer();


