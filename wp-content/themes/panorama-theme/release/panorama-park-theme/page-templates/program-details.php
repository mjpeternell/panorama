<?php
/**
 * Template Name: Program Details
 * The template used for displaying page content in page.php
 *
 * @package Panorama
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-parallax');
?>
<?php get_template_part('inc/page-template-intro-content'); ?>
<?php
$our_role_content_id = "";
if (get_field('our_role_content_id')) {
    $our_role_content_id = get_field('our_role_content_id');
}

$our_role_section_title = "";
if (get_field('our_role_section_title')) {
    $our_role_section_title = get_field('our_role_section_title');
}

$our_perspective_section_id = "";
if (get_field('our_perspective_section_id')) {
    $our_perspective_section_id = get_field('our_perspective_section_id');
}

$our_perspective_section_title = "";
if (get_field('our_perspective_section_title')) {
    $our_perspective_section_title = get_field('our_perspective_section_title');
}

$about_section_id = "";
if (get_field('about_section_id')) {
    $about_section_id = get_field('about_section_id');
}

$about_section_title = "";
if (get_field('about_section_title')) {
    $about_section_title = get_field('about_section_title');
}
$resources_section_id = "";
if (get_field('resources_section_id')) {
    $resources_section_id = get_field('resources_section_id');
}
if (get_field('resources_section_title')) {
    $resources_section_title = get_field('resources_section_title');
}
?>

<nav id="pageAuxNav" class="planetary-health-nav ncol-fullbleed">
    <div class="col-full">
        <div class="column-10 offset-1">
            <ul  class="aux-nav-menu">
                <?php if (get_field('our_role_content_id')) { ?>
                    <li><a href="#<?php echo $our_role_content_id; ?>" class="" title="<?php echo $our_role_section_title; ?>"><?php echo $our_role_section_title; ?></a></li>
                <?php } ?>
                <?php if (get_field('our_perspective_section_id')) { ?>
                    <li><a href="#<?php echo $our_perspective_section_id; ?>" class="" title="<?php echo $our_perspective_section_title; ?>"><?php echo $our_perspective_section_title; ?></a></li>
                <?php } ?>
                <?php if (get_field('about_section_id')) { ?>
                    <li><a href="#<?php echo $about_section_id; ?>" class="" title="<?php echo $about_section_title; ?>"><?php echo $about_section_title; ?></a></li>
                <?php } ?>
                <?php if (get_field('resources_section_id')) { ?>
                    <li><a href="#<?php echo $resources_section_id; ?>" class="" title="<?php echo $resources_section_title; ?>">Resources</a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>

<?php if ($our_role_content_id): ?>
    <section id="<?php echo $our_role_content_id; ?>" class="role-section col-fullbleed">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php if (get_field('our_role_section_title')) { ?>
                    <h2 class="section-title"><?php echo $our_role_section_title; ?></h2>
                <?php } ?>
                <?php
                if (get_field('our_role_content')) {
                    echo $our_role_content = get_field('our_role_content');
                }
                ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($our_perspective_section_id): ?>
    <section id="<?php echo $our_perspective_section_id; ?>" class="perspective-section col-fullbleed">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php if (get_field('our_perspective_section_title')) { ?>
                    <h2 class="section-title"><?php echo $our_perspective_section_title; ?></h2>
                <?php } ?>
                <?php
                if (get_field('our_perspective_content')) {
                    echo $our_perspective_content = get_field('our_perspective_content');
                }
                ?>
            </div>
        </div>
        <div class="col-full perspective-downloads">
            <div class="column-10 offset-1">              
                <?php if (have_rows('our_perspective_document_repeater')): ?> 
                    <h3>DOWNLOADS</h3>
                    <div class="download-links-a">
                        <?php
                        $link_count = 0;
                        while (have_rows('our_perspective_document_repeater')): the_row();
                            $link_count++;
                            $link_url = get_sub_field('perspective_link_url');
                            if ($link_url) {
                                $my_id2 = $link_url['ID'];
                                $url_file = $link_url['url'];
                                $title_file = $link_url['title'];
                                $caption_file = $link_url['caption'];
                                $desc = $link_url['description'];
                                $icon = $link_url['icon'];
                            }
                            ?>

                            <div class="column-2a doc-icon" data-tile-count="<?php echo $link_count; ?>">
                                <?php if (wp_get_attachment_image($link_url['ID'])) { ?>
                                    <a href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>">
                                        <?php echo wp_get_attachment_image($link_url['ID'], 'thumbnail', "", array("class" => "img-thumbnail")); ?>
                                    </a>

                                <?php } else { ?>
                                    <a href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>">
                                        <img src="/wp-content/themes/panorama-theme/assets/images/pdf-icon.png" class="img-responsive"/>
                                    </a>
                                <?php } ?>

                                <?php if ($caption_file) { ?>
                                    <a class="caption" href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>">
                                        <?php echo $caption_file; ?>
                                    </a>
                                <?php } ?>
                            </div>
                            <?php
//                            if($link_count % 6 === 0) {
//                                echo '</div><div class="download-links-a">';
//                            }
                            ?>
                        <?php endwhile; ?>
                    </div>

                <?php endif; ?>

            </div>
        </div>
        <?php
        $my_pov_term = "";
        if (get_field('our_perspective_pov_blog_tag')) {
            $pov_term = get_field('our_perspective_pov_blog_tag');
            $my_pov_term = $pov_term->slug;
        }
        $persp_arg = array(
            'post_type' => 'point_of_view',
            'orderby' => 'date',
            'order' => 'date',
            'post_status' => 'publish',
            //'tag' => $my_pov_term,
            'tax_query' => array(
                array(
                    'taxonomy' => 'pov_categories',
                    'field' => 'slug',
                    'terms' => $my_pov_term,
                )
            ),
            'posts_per_page' => 3,
        );
        $wp_persp_query = new WP_Query($persp_arg);
        $postx_counter = -1;
        if (have_posts()) :
            ?>
            <div class="col-full">
                <div class="column-10 offset-1">
                    <h3>RECENT PANORAMA BLOG POSTS</h3>
                    <?php
                    while ($wp_persp_query->have_posts()) : $wp_persp_query->the_post();
                        $image_id = get_post_thumbnail_id();
                        $imagesize = "thumbnail";

                        $postx_counter++;
                        ?>
                        <div class="blog-column-4 blog-feature-tile" data-count="<?php echo $postx_counter; ?>">
                            <article class="tile-inner" >
                                <div class="blog-image">
                                    <?php
                                    if (has_post_thumbnail($wp_persp_query->ID)) {
                                        echo '<a href="' . get_permalink($wp_persp_query->ID) . '" title="' . esc_attr($wp_persp_query->post_title) . '">';
                                        echo get_the_post_thumbnail($wp_persp_query->ID, 'hero-cta-680');
                                        echo '</a>';
                                    } else {
                                        echo '<a href="' . get_permalink($wp_persp_query->ID) . '" title="' . esc_attr($wp_persp_query->post_title) . '">';
                                        echo '<img src="/wp-content/themes/panorama-theme/assets/images/panorama-blog-placeholder-image.jpg" class="img-responsive"  alt="PlaceHolder Image"/>';
                                        echo '</a>';
                                    }
                                    ?>
                                </div>
                                <header class="entry-header">
                                    <h1 class="title"><a href="<?php the_permalink(); ?>" class="trunc-115"><?php the_title(); ?></a></h1>
                                    <?php
                                    //$author = get_the_author();
                                    //echo '<span class="author">By: ' . $author . '</span>';
                                    ?> 
                                    <?php
                                    $post_pen_name = get_field('post_pen_name', $post->ID);
                                    if ($post_pen_name) :
                                        ?>
                                        <span class="author"><?php echo $post_pen_name; ?></span>
                                    <?php else: ?>
                                        <span class="author">&nbsp;</span>
                                    <?php endif; ?>
                                    <?php
                                    $post_source_name = get_field('post_source_name', $post->ID);
                                    if ($post_source_name) :
                                        ?>
                                        <span class="author"><?php echo $post_source_name; ?></span>
                                    <?php else: ?>
                                        <span class="author">&nbsp;</span>
                                    <?php endif; ?>
                                </header>
                                <!--                            <div class="blog-content">
                                <?php //the_excerpt();     ?>
                                                            </div>-->

                                <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<footer class="entry-meta">', '</footer>', 0, 'post-edit-link btn btn-default'); ?>

                            </article>
                        </div>
                    <?php endwhile;
                    ?>
                </div>
            </div>
            <?php
        endif;
        ?>


        <?php wp_reset_postdata(); ?>
    </section>
<?php endif; ?>

<?php if ($about_section_id): ?>
    <section id="<?php echo $about_section_id; ?>" class="about-section col-fullbleed">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php if (get_field('about_section_title')) { ?>
                    <h2 class="section-title"><?php echo $about_section_title; ?></h2>
                <?php } ?>
                <?php
                if (get_field('about_section_content')) {
                    echo $about_section_content = get_field('about_section_content');
                }
                ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($resources_section_id): ?>
    <section id="<?php echo $resources_section_id; ?>" class="resource-section col-fullbleed">
        <div class="col-full">
            <div class="column-10 offset-1">
                <?php if (get_field('resources_section_title')) { ?>
                    <h3 class="section-sub-title"><?php echo $resources_section_title; ?></h3>
                <?php } ?>
                <?php
                if (get_field('resources_section_content')) {
                    echo $resources_section_content = get_field('resources_section_content');
                }
                ?>

                <?php if (have_rows('resources_link_repeater')): ?>
                    <ul class="resource-slides">
                        <?php
                        while (have_rows('resources_link_repeater')): the_row();
                            $resouce_link_label = get_sub_field('resouce_link_label');
                            $resouce_link_url = get_sub_field('resouce_link_url');
                            $resource_extra_info = get_sub_field('resource_extra_info');
                            ?>
                            <li class="resource-slide">
                                <?php if ($resouce_link_url): ?>
                                    <a class="link-url" href="<?php echo $resouce_link_url; ?>" target="_blank" title="<?php echo $resouce_link_label; ?>"><?php echo $resouce_link_label; ?></a>
                                    <?php if ($resouce_link_url): ?><span class="link-info"><?php echo $resource_extra_info; ?></span><?php endif; ?>
                                <?php endif; ?>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
                <?php if (have_rows('resources_document_repeater')): ?> 
                    <hr>
                    <ul class="download-links">
                        <?php
                        while (have_rows('resources_document_repeater')): the_row();
                            $link_url = get_sub_field('resouce_link_url');
                            if ($link_url) {
                                $my_id2 = $link_url['ID'];
                                $url_file = $link_url['url'];
                                $title_file = $link_url['title'];
                                $caption_file = $link_url['caption'];
                                $desc = $link_url['description'];
                                $icon = $link_url['icon'];
                            }
                            ?>
                            <li class="link-item column-12 ">
                                <div class="column-6">

                                    <a class="title" href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>"><?php echo $title_file; ?></a>
                                    <?php if ($desc) { ?>
                                        <div class="descrition"><?php echo $desc; ?></div>
                                    <?php } ?>
                                </div>
                                <div class="column-6 doc-icon">
                                    <?php if (wp_get_attachment_image($link_url['ID'])) { ?>
                                        <a href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>">
                                            <?php echo wp_get_attachment_image($link_url['ID'], 'thumbnail', "", array("class" => "img-thumbnail")); ?>
                                        </a>

                                    <?php } else { ?>
                                        <a href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>">
                                            <img src="/wp-content/themes/panorama-theme/assets/images/pdf-icon.png" class="img-responsive"/>
                                        </a>
                                    <?php } ?>

                                    <?php if ($caption_file) { ?>
                                        <a class="caption" href="<?php echo $url_file; ?>" target="_blank" title="<?php echo $title_file; ?>">
                                            <?php echo $caption_file; ?>
                                        </a>
                                    <?php } ?>
                                </div>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php get_template_part('inc/panorama', 'contact-cta'); ?>
<?php
get_footer();


