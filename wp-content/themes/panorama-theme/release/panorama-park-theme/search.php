<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">

            <?php if (have_posts()) : ?>
                <div class="column-12 text-center">
                    <header class="page-header">
                        <h1 class="page-title"><?php printf(__('Search Results for: %s', 'panorama'), '<span>' . get_search_query() . '</span>'); ?></h1>
                    </header><!-- .page-header -->

                </div>   
                <div class="column-10 offset-1 search-list top">
                    <?php //panorama_content_nav('nav-above'); ?>
                    <?php /* Start the Loop */ ?>
                    <?php while (have_posts()) : the_post(); ?>

                        <?php get_template_part('content', 'search'); ?>

                    <?php endwhile; ?>

                    <?php panorama_content_nav('nav-below'); ?>
                </div>
                <div class="column-10 offset-1 search-list bot">
                    <div class="blog-column-6">
                        <div class="inpage-dropdown">
                            <div class="dropdown-container">
        <!--                        <p class="dropdown-description">Topics</p>-->
                                <div class="dropdown-button">Topics</div>
                                <ul class="dropdown-menu dropdown-select">
                                    <?php
                                    $args = array(
                                        'hide_empty' => 1,
                                        'number' => 10,
                                        'order' => 'DESC',
                                        'orderby' => 'count'
                                    );

                                    $terms = get_terms('pov_tags', $args);

                                    if (!empty($terms) && !is_wp_error($terms)) {
                                        $count = count($terms);
                                        $i = 0;
                                        $term_list = '';
                                        foreach ($terms as $term) {
                                            $i++;
                                            $term_list .= '<li><a href="' . esc_url(get_term_link($term)) . '" alt="' . esc_attr(sprintf(__('View all post filed under %s', 'my_localization_domain'), $term->name)) . '">' . $term->name . '<span class="myCount">(' . $term->count . ')</span></a></li>';
                                        }
                                        echo $term_list;
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="blog-column-6">
                        <?php get_template_part('template-parts/panorama', 'search-form'); ?>
                    </div>
                </div>
                <?php else : ?>
                    <?php get_template_part('template-parts/no-results', 'search'); ?>      
                <?php endif; ?>

            </div>
        <?php get_template_part('inc/panorama', 'contact-cta'); ?>
    </main>
</div>

<?php get_footer(); ?>