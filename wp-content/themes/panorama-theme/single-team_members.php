<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="single-hero">
                        <div class="single-caption">
                            <h1 class="single-title"><?php the_title(); ?></h1>
                            <?php
                            $team_member_title = get_field("team_member_title");
                            if ($team_member_title) {
                                ?>
                                <div class="single-subtitle"><?php echo $team_member_title; ?></div>
                            <?php } else { ?>
                                <div class="single-subtitle">&nbsp;</div>
                            <?php }?>
                            <div class="single-team-img"><?php the_post_thumbnail(); ?></div>
                        </div>
                    </header><!-- .entry-header -->
                    <div class="column-8 offset-2 single-team-post">
                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php
                            $team_member_long_bio = get_field("team_member_long_bio");
                            if ($team_member_long_bio) {
                                ?>
                                <?php echo $team_member_long_bio; ?>
                            <?php } ?>
                            <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'panorama'), 'after' => '</div>')); ?>
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
                            <?php edit_post_link(__('Edit', 'panorama'), '<span class="edit-link">', '</span>'); ?>
                        </footer><!-- .entry-meta -->
                    </div>
                    <div class="column-8 offset-2 align-center">
                        <div class="single-team-nav bottom">



                            <?php
                            $link_id = "";
                            $link_string = "";
                            $terms = get_the_terms($post->ID, 'team_category');
                            if ($terms && !is_wp_error($terms)) :
                                $tslugs_arr = array();
                                foreach ($terms as $term) {
                                    $tslugs_arr[] = $term->slug;
                                }
                                $terms_slug_str = join(" ", $tslugs_arr);
                            endif;
            
                            if ($terms_slug_str === "advisor") {
                                $link_id = "/about/#team";
                                $link_string = "Advisors";
                            }
                            if ($terms_slug_str === "team") {
                                $link_id = "/about/#team";
                                $link_string = "Team Members";
                            }
                            if ($terms_slug_str === "ceo") {
                                $link_id = "/about/#team";
                                $link_string = "Team Members";
                            }
                            ?>


                            <a id="teamLink" href="<?php echo $link_id; ?>" type="button" class=""><i class="fa fa-angle-left" aria-hidden="true"></i> Back To <?php echo $link_string; ?></a>
                        </div>
                    </div>
                </article><!-- #post-<?php the_ID(); ?> -->
            <?php endwhile; // end of the loop.   ?>
        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_template_part('inc/panorama', 'contact-cta'); ?>
<?php
//get_template_part('inc/footer-cta-single');
get_footer();
?>