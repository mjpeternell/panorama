<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Panorama
 * @since Panorama 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="single-hero">
                        <div class="single-caption">
                            <h1 class="single-title trunc-175"><?php the_title(); ?></h1>
                            <div class = "single-subtitle">
                                <?php // the_author(); ?> 
                                <?php
                                $post_pen_name = get_field('post_pen_name');
                                if ($post_pen_name) :
                                    ?>
                                    <span class="author">By <?php echo $post_pen_name; ?></span>
                                <?php else: ?>
                                    <span class="author">&nbsp;</span>
                                <?php endif; ?>
                                <?php
                                $post_source_name = get_field('post_source_name', $post->ID);
                                if ($post_source_name) :
                                    ?>
                                    <span class="author"><?php echo $post_source_name; ?></span>
                                <?php else: ?>
                                    <span class="author">&nbsp;</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </header><!-- .entry-header -->
                    <div class="column-8 offset-2 single-pov-post">
                       <?php echo do_shortcode( '[foobar]' ); ?>
                        <?php
                        // check if the post has a Post Thumbnail assigned to it.
                        if (has_post_thumbnail()) :
                            ?>
                            <div class="featured-img">
                                <?php
                                $get_description = get_post(get_post_thumbnail_id())->post_excerpt;
                                the_post_thumbnail();
                                if (!empty($get_description)) {//If description is not empty show the div
                                    echo '<p class="wp-caption-text">' . $get_description . '</p>';
                                }
                                ?>
                            </div>
                            <?php endif; ?>
                        <div class="entry-content">   
                            <?php the_content();?>
                            
    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'panorama'), 'after' => '</div>')); ?>
                        </div><!-- .entry-content -->
                        <footer class="entry-meta">
    <?php panorama_content_nav('nav-below'); ?>
                            <ul class="pov-cat-nav">
                                <li><span>Go to:</span></li>
                                <li><a href="/point-of-view/">Point of View</a></li>
                                <li><a href="/pov_categories/planet/">Planet</a></li>
                                <li><a href="/pov_categories/people/">People</a></li>
                                <li><a href="/pov_categories/productivity/">Productivity</a></li>
                            </ul>
    <?php edit_post_link(__('Edit', 'panorama'), '<span class="edit-link">', '</span>'); ?>
                        </footer><!-- .entry-meta -->
                    </div>
                </article><!-- #post-<?php the_ID(); ?> -->
<?php endwhile; // end of the loop.       ?>
        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php get_template_part('inc/panorama', 'contact-cta'); ?>
<?php get_footer(); ?>